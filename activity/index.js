let http = require('http');

let port = 4000;

let courses = [
	{
		"course": "JavaScript",
		"description": "asdfasdf"
	}
];

let server = http.createServer(function(req, res) {
	if(req.url == '/profile' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'text/plain'});
		res.end('Welcome to your profile');
	} else if (req.url == '/courses' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end("Here's our courses available");
	} else if (req.url == '/addcourse' && req.method == 'POST') {
		let request_body = '';
		req.on('data', function(data){
			request_body += data;
		});
		req.on('end', function(data){
			request_body = JSON.parse(request_body);
			let newCourse = {
				"course": request_body.course,
				"description": request_body.description
			}
			courses.push(newCourse);
			res.writeHead(200, {'Content-Type': 'application/json'});
			// res.writeHead(200, {'Content-Type': 'text/plain'});
			res.write(JSON.stringify(newCourse));
			res.end('Add a course to our resources');
		})		
	} else if (req.url == '/' && req.method == 'GET') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end('Welcome to Booking System');
	} else if (req.url == '/updatecourse' && req.method == 'PUT') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end('Update a course to our resource');
	} else if (req.url == '/archivecourse' && req.method == 'DELETE') {
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.end('Archive courses to our resource');
	};
});

server.listen(port);

console.log(`Server is running at localhost: ${port}`);